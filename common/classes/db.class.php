<?php

class Db {

    private $_connection;
    private static $_instance; //The single instance
    private $_host = "localhost";
    private $_username = "root";
    private $_password = "";
    private $_database = "webappmaker";

    
    // Constructor
    private function __construct() {
        $this->_connection = new mysqli($this->_host, $this->_username, $this->_password, $this->_database);
        
        // Error handling.
        if (mysqli_connect_error()) {
            trigger_error('Failed to connect to MySQL: ' . mysqli_connect_error(), E_USER_ERROR);
        }
    }

    /*
      Get an instance of the Database
      @return Instance
     */
    public static function getInstance() {
        if (!self::$_instance) { // If no instance then make one
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function close(){
       if (self::$_instance) {
           self::$_instance->getConnection()->close();
           self::$_instance = null;
       }
    } 
    
    // Get mysqli connection
    public function getConnection() {
        return $this->_connection;
    }

    public function insert($table, $data) {

        $db = self::getInstance();
        $mysqli = $db->getConnection();

        $fields = array();
        $values = array();
        foreach ($data as $key => $value) {
            $fields[] = "`$key`";
            $values[] = "'$value'";
        }

        $sql = "INSERT INTO " . $table . "  ( " . implode(',', $fields) . ") VALUES ( " . implode(',', $values) . " )";

        $query = $mysqli->query($sql) or die(mysql_error());

        return $mysqli->insert_id;
    }
    
    public function getQueryObj( $sql, $type=NULL ){
        
        $db = self::getInstance();
        $mysqli = $db->getConnection();
        
        $query = $mysqli->query($sql) or die(mysql_error());
        
        switch ($type) {
            case 'select':
                return $query;
                break;
            case 'insert':
                return $mysqli->insert_id;
                 break;
            case 'update':
                return $query;
                break;
            default:
                return $query;
                break;
                
        }
        
        
    }

    public function update($table, $data, $where) {

        $db = self::getInstance();
        $mysqli = $db->getConnection();

        $field = array();
        $values = array();
        $set = array();

        foreach ($data as $key => $value) {
            $field[] = $key;
            $values[] = $value;
            $set[] = "`$key`" . "=" . "'$value'";
        }
        $set = implode(", ", $set);
        $sql = "UPDATE $table  SET $set  WHERE $where";

        $query = $mysqli->query($sql) or die(mysql_error());

        return $query;
    }

    public function selectAll($table, $where = '1', $limit = NULL) {

        $db = self::getInstance();
        $mysqli = $db->getConnection();
        if ($limit != NULL) {
            $limit = "LIMIT $limit";
        } else {
            $limit = "";
        }
        $sql = "SELECT * FROM $table WHERE $where  $limit";        
        $query = $mysqli->query($sql) or die(mysql_error());
        
        return $query;
    }

    public function select($table, $fields, $where = '1') {

        $db = self::getInstance();
        $mysqli = $db->getConnection();

        $fieldsStr = implode(',', $fields);

        $sql = "SELECT $fieldsStr FROM $table WHERE $where";

        $query = $mysqli->query($sql) or die(mysql_error());

        return $query;
    }
    
    public function selectColumn( $table ) {

        $db = self::getInstance();
        $mysqli = $db->getConnection();

        $sql = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '".$table."' AND table_schema = 'webappmaker' ";
        $query = $mysqli->query($sql) or die(mysql_error());

        return $query;
    }

    public function delete($table, $where) {

        $query = mysql_query("DELETE FROM $table WHERE  $where");

        return $query;
    }
    
    
    

}
