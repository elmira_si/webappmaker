<?php

class Actions{
    
    public function __construct() {
        
    }
    
    public function saveRow( $table, $data ) {        
        $db = Db::getInstance();        
        $insertID = $db->insert($table, $data); 
        
        return $insertID;
    }
    
    public function saveForm( $data ) {        
        $db = Db::getInstance();        
        $insertID = $db->insert('forms', $data); 
        
        return $insertID;
    }
    
    public function updateForm( $data, $id ) {        
        $db = Db::getInstance();     
        $where = "ID = $id";
        $updateRow = $db->update('forms', $data, $where ); 
        
        return $updateRow;
    }
    
    public function updateRow( $table, $data, $id ) {        
        $db = Db::getInstance();     
        $where = "ID = $id";
        $updateRow = $db->update( $table, $data, $where ); 
        
        return $updateRow;
    }
    
    public function getFields($table, $fields, $where = '1') {
        $db = Db::getInstance();        
        $data = $db->select($table, $fields, $where);        
        return $data;
    }
    
    public function getAll($table) {
        $db = Db::getInstance();        
        $data = $db->selectAll($table);
        
        return $data;
    }
    
    public function getFormData($formId) {
        $db = Db::getInstance();        
        $formData = $db->selectAll("forms", "ID = $formId");
        
        return $formData->fetch_assoc();
    }
    
    public function getRowData( $table, $pageId) {
        $db = Db::getInstance();        
        $formData = $db->selectAll( $table, "ID = $pageId");
        
        return $formData->fetch_assoc();
    }
    
}