<?php

class page{
    
    public function __construct() {
        
    }
    
    public function getPage($id) {
        
        $db = Db::getInstance();     
        $where = "ID = $id";
        $getPage = $db->selectAll('pages', $where ); 
        $page = $getPage->fetch_object();
        
        return $page;
        
    }
    
    public function getShortcode($content){
        
        if ( false === strpos( $content, '[' ) ) {
		return $content;
	}
        preg_match_all( '@\[([^<>&/\[\]\x00-\x20=]++)@', $content, $matches );
	
        $tagName = $matches[1];
        
        if ( empty( $tagName ) ) {
            return $content;
	}
        $pattern = $this->get_shortcode_regex( $tagName );
        
        $content = preg_replace_callback( "/$pattern/", array($this, 'do_shortcode_tag'), $content );
        
	// Always restore square braces so we don't break things like <!--[if IE ]>
	$content = $this->unescape_invalid_shortcodes( $content );
	
        return $content;
    }
    
    function do_shortcode_tag( $m ){
        
        // allow [[foo]] syntax for escaping a tag
	if ( $m[1] == '[' && $m[6] == ']' ) {
            return substr($m[0], 1, -1);
	}
	$tag = $m[2];
	$attr = $this->shortcode_parse_atts( $m[3] );
        
        $content = isset( $m[5] ) ? $m[5] : null;
        
        $output = $m[1] . call_user_func( array($this,$m[2]), $attr, $content, $tag) . $m[6];
        
        return $output;
    }  
           
    function unescape_invalid_shortcodes( $content ) {
        // Clean up entire string, avoids re-parsing HTML.
        $trans = array( '&#91;' => '[', '&#93;' => ']' );
        $content = strtr( $content, $trans );
        return $content;
    }

    function get_shortcode_regex( $tagnames = null ) {
        global $shortcode_tags;
        if ( empty( $tagnames ) ) {
                $tagnames = array_keys( $shortcode_tags );
        }
        $tagregexp = join( '|', array_map('preg_quote', $tagnames) );
        // WARNING! Do not change this regex without changing do_shortcode_tag() and strip_shortcode_tag()
        // Also, see shortcode_unautop() and shortcode.js.
        return
                  '\\['                              // Opening bracket
                . '(\\[?)'                           // 1: Optional second opening bracket for escaping shortcodes: [[tag]]
                . "($tagregexp)"                     // 2: Shortcode name
                . '(?![\\w-])'                       // Not followed by word character or hyphen
                . '('                                // 3: Unroll the loop: Inside the opening shortcode tag
                .     '[^\\]\\/]*'                   // Not a closing bracket or forward slash
                .     '(?:'
                .         '\\/(?!\\])'               // A forward slash not followed by a closing bracket
                .         '[^\\]\\/]*'               // Not a closing bracket or forward slash
                .     ')*?'
                . ')'
                . '(?:'
                .     '(\\/)'                        // 4: Self closing tag ...
                .     '\\]'                          // ... and closing bracket
                . '|'
                .     '\\]'                          // Closing bracket
                .     '(?:'
                .         '('                        // 5: Unroll the loop: Optionally, anything between the opening and closing shortcode tags
                .             '[^\\[]*+'             // Not an opening bracket
                .             '(?:'
                .                 '\\[(?!\\/\\2\\])' // An opening bracket not followed by the closing shortcode tag
                .                 '[^\\[]*+'         // Not an opening bracket
                .             ')*+'
                .         ')'
                .         '\\[\\/\\2\\]'             // Closing shortcode tag
                .     ')?'
                . ')'
                . '(\\]?)';                          // 6: Optional second closing brocket for escaping shortcodes: [[tag]]
    }
    
    public function shortcode_parse_atts($text) {
        
        $atts = array();
	$pattern = $this->get_shortcode_atts_regex();
	$text = preg_replace("/[\x{00a0}\x{200b}]+/u", " ", $text);
	if ( preg_match_all($pattern, $text, $match, PREG_SET_ORDER) ) {
		foreach ($match as $m) {
			if (!empty($m[1]))
				$atts[strtolower($m[1])] = stripcslashes($m[2]);
			elseif (!empty($m[3]))
				$atts[strtolower($m[3])] = stripcslashes($m[4]);
			elseif (!empty($m[5]))
				$atts[strtolower($m[5])] = stripcslashes($m[6]);
			elseif (isset($m[7]) && strlen($m[7]))
				$atts[] = stripcslashes($m[7]);
			elseif (isset($m[8]))
				$atts[] = stripcslashes($m[8]);
		}
		// Reject any unclosed HTML elements
		foreach( $atts as &$value ) {
			if ( false !== strpos( $value, '<' ) ) {
				if ( 1 !== preg_match( '/^[^<]*+(?:<[^>]*+>[^<]*+)*+$/', $value ) ) {
					$value = '';
				}
			}
		}
	} else {
		$atts = ltrim($text);
	}
	return $atts;
    }
    
    function get_shortcode_atts_regex() {
        return '/([\w-]+)\s*=\s*"([^"]*)"(?:\s|$)|([\w-]+)\s*=\s*\'([^\']*)\'(?:\s|$)|([\w-]+)\s*=\s*([^\s\'"]+)(?:\s|$)|"([^"]*)"(?:\s|$)|(\S+)(?:\s|$)/';
    }
    
    //shortcode call_user_func familyTree
    function familyTree($attr, $content, $tag){
        return '';
    }
    
    //shortcode call_user_func browseForm
    function browseForm($attr, $content, $tag){
        
        extract($attr);
               
        
        require_once ( $_SERVER["DOCUMENT_ROOT"] ."/frontend/includes/templates/browseForm.php");
    }
    
    
    function getBrowseFormLabels($pageID){
        
        $db = Db::getInstance();     
        $where = "ID = $pageID";
        $getPage = $db->selectAll('bforms', $where );
        $getPage = $getPage->fetch_object();
        $sql = $getPage->query;       
        
        $result = $db->getQueryObj($sql);
        //$fieldCount = $result->field_count;
       
        if( $result ){
            $i = 1;
            $labels = array();
            while ($obj = $result->fetch_object()) {
                if( $i == 1 ){
                    foreach ( $obj as $k => $v ){
                        $labels[] = array('label'=>$k,'name'=>$k,'width'=>'150','editable'=>true);
                    }
                }
               
                $i++;
            }              
            $result->close();
        }        
        $db->close();
        
        /*$labels = array(
            '0' => array('label'=>'Employee ID','name'=>'EmployeeID','width'=>'150','editable'=>true),
            '1' => array('label'=>'First Name','name'=>'FirstName','width'=>'150','editable'=>true),
            '2' => array('label'=>'Last Name','name'=>'LastName','width'=>'150','editable'=>true),
            '3' => array('label'=>'City','name'=>'City','width'=>'150','editable'=>true)
        );*/
        
        return json_encode($labels);
    }
    
    function getBrowseFormData( $pageID ){
        
        $db = Db::getInstance();     
        $where = "ID = $pageID";
        $getPage = $db->selectAll('bforms', $where );
        $getPage = $getPage->fetch_object();
        $sql = $getPage->query;       
        
        $data = $db->getQueryObj($sql);
        //$fieldCount = $result->field_count;
       
        
        
       /*$data = array(				
            '0' => array('EmployeeID'=>'1', 'FirstName'=>'myFna', 'LastName' => 'aas', 'City' => 'City'),
            '1' => array('EmployeeID'=>'1', 'FirstName'=>'myFna', 'LastName' => 'aas', 'City' => 'City'),
            '2' => array('EmployeeID'=>'1', 'FirstName'=>'myFna', 'LastName' => 'aas', 'City' => 'City'),
            '3' => array('EmployeeID'=>'1', 'FirstName'=>'myFna', 'LastName' => 'aas', 'City' => 'City'),
            '4' => array('EmployeeID'=>'1', 'FirstName'=>'myFna', 'LastName' => 'aas', 'City' => 'City'),
            '5' => array('EmployeeID'=>'1', 'FirstName'=>'myFna', 'LastName' => 'aas', 'City' => 'City'),
            '6' => array('EmployeeID'=>'1', 'FirstName'=>'myFna', 'LastName' => 'aas', 'City' => 'City'),
            '7' => array('EmployeeID'=>'1', 'FirstName'=>'myFna', 'LastName' => 'aas', 'City' => 'City'),
            '8' => array('EmployeeID'=>'1', 'FirstName'=>'myFna', 'LastName' => 'aas', 'City' => 'City'),
            '9' => array('EmployeeID'=>'1', 'FirstName'=>'myFna', 'LastName' => 'aas', 'City' => 'City')
	);*/
       
       return json_encode($data);
    }
    
    
    
}