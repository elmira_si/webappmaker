<?php

class Init{
    
    public $layout = 'main';
    protected $controller = 'home';
    protected $method = 'index';
    protected $params = [];
	

    public function __construct( $config=NULL, $commonFunctions =NULL )
    {      
        
        if ( $config ) {
            require_once $config;
        }
        if ($commonFunctions){
            require_once $commonFunctions;
        }
                  
        require_once '../common/classes/db.class.php';   
        require_once '../common/includes/functions.php';   
        require_once 'Page.php'; 
        require_once 'Actions.php'; 
        require_once 'includes/index.php';        
         
        //self::testDB();
    }
    
   

    
	
}
