<br /><br />
<style type="text/css">
    .ui-jqgrid-view {}
</style>

<input type="button" value="Edit in Batch Mode" onclick="startEdit()" />
<input type="button" value="Save All Rows" onclick="saveRows()" />

<br /><br />

<table id="jqGrid"></table>
<div id="jqGridPager"></div>

<script type="text/javascript">
    
    

    $(document).ready(function () {
        
        
        $.ajax({
            url: "pageAjaxHandler.php",
            dataType: "json",
            method: "POST",
            data: {
                browseFormLabels: 1,
                pageID: 1
            },
            success: function (result) {                
                //console.log(result);
                $("#jqGrid").jqGrid({            
                    colModel: result,
                    sortname: 'EmployeeID',
                    loadonce: true,
                    viewrecords: true,
                    //width: 500,
                    height: 400,
                    rowNum: 5,
                    datatype: 'local',
                    pager: "#jqGridPager"
                });
                
                fetchGridData();
            }
        });       
        

        function fetchGridData() {

            var gridArrayData = [];
            // show loading message
            $("#jqGrid")[0].grid.beginReq();
            $.ajax({
                url: "pageAjaxHandler.php",
                dataType: "json",
                method: "POST",
                data: {
                    browseFormId: 1,
                    pageID: 1
                },
                success: function (result) {
                    console.log(result);
                    for (var i = 0; i < result.length; i++) {
                        var item = result[i];
                        gridArrayData.push({
                            EmployeeID: item.EmployeeID,
                            FirstName: item.FirstName,
                            LastName: item.LastName,
                            City: item.City,
                        });                            
                    }
                    // set the new data
                    $("#jqGrid").jqGrid('setGridParam', { data: gridArrayData});
                    // hide the show message
                    $("#jqGrid")[0].grid.endReq();
                    // refresh the grid
                    $("#jqGrid").trigger('reloadGrid');
                }
            });
        }

            function formatTitle(cellValue, options, rowObject) {
                return cellValue.substring(0, 50) + "...";
            };

            function formatLink(cellValue, options, rowObject) {
                return "<a href='" + cellValue + "'>" + cellValue.substring(0, 25) + "..." + "</a>";
            };

        
    });

    function startEdit() {
        var grid = $("#jqGrid");
        var ids = grid.jqGrid('getDataIDs');
        for (var i = 0; i < ids.length; i++) {
            grid.jqGrid('editRow', ids[i]);
        }
    }

    function saveRows() {
        var grid = $("#jqGrid");
        var ids = grid.jqGrid('getDataIDs');

        for (var i = 0; i < ids.length; i++) {
            grid.jqGrid('saveRow', ids[i]);
        }
    }

</script>