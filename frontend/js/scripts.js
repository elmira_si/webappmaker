 $(document).ready(function(){  
     
    
    $( "#tabs, #builder-options" ).tabs();

    $('#editor, .row-content').sortable();
    
    var gridClasses = [
                        'grid_one',
                        'grid_five_sixth',
                        'grid_four_fifth',
                        'grid_three_fourth',
                        'grid_two_third',
                        'grid_three_fifth',                        
                        'grid_two',
                        'grid_two_fifth',                        
                        'grid_three',                         
                        'grid_four',                        
                        'grid_five',
                        'grid_six',
                    ];
                    
    var gridObject = {
                        'grid_one': '1/1',
                        'grid_five_sixth': '5/6',
                        'grid_four_fifth': '4/5',
                        'grid_three_fourth': '3/5',
                        'grid_two_third': '2/3',
                        'grid_three_fifth': '3/4',                        
                        'grid_two': '1/2',
                        'grid_two_fifth': '2/5',                        
                        'grid_three': '1/3',                         
                        'grid_four': '1/4',                        
                        'grid_five': '1/5',
                        'grid_six': '1/6',
                    };
                    
    //filtering data attribute
    $.fn.filterByData = function(prop, val) {
        var $self = this;
        if (typeof val === 'undefined') {
            return $self.filter(  function() {
              return typeof $(this).data(prop) !== 'undefined';
            });
        }
        return $self.filter( function() {
          return $(this).data(prop) == val;
        });
    };
    
    /* Get the value of an inline style property 
    * return undefined if that property is not found
    * var width = $("#someElem").inlineStyle("width"); 
    * //Returns value of "width" property or `undefined`
    * */
    $.fn.inlineStyle = function (prop) {
         var styles = this.attr("style"),
             value;
         styles && styles.split(";").forEach(function (e) {
             var style = e.split(":");
             if ($.trim(style[0]) === prop) {
                 value = style[1];           
             }                    
         });   
         return value;
    }; 
   
    $('#preview').on('click', function(){
        var formTitle = $('#form-title').val();
        var formContent = $('#html-content').val();
        
        $('#form-preview-modal .title').text(formTitle);
        $('#form-preview-modal .modal-body').html(formContent);
        
    });
    
    //decrease or increase column width
    $(document).delegate('.change-width', 'click', function(){
        var currClass = $(this).parent().parent().attr('class');
        var title;
        currClass = currClass.match(/grid_[\w-]*\b/);
        currClass = currClass[0]; 
        
        var objValNum = gridObject[currClass];
     
        for( var i = 0; i < gridClasses.length; i++ ){
            if( gridClasses[i] == currClass ){                
               if( $(this).hasClass('increase-width') ){
                   if ( i < 11 ){       
                       $(this).parent().parent().removeClass(currClass).addClass(gridClasses[i+1]);
                       title = $('.custom-layout-cols').find('div[data-id="'+gridClasses[i+1]+'"]').attr('title');
                       $(this).parent().find('.grid_width').text(title);
                       updateHtmlContent();
                   }
               } else {
                    if ( i > 0 ){
                        $(this).parent().parent().removeClass(currClass).addClass(gridClasses[i-1]);
                        title = $('.custom-layout-cols').find('div[data-id="'+gridClasses[i-1]+'"]').attr('title');
                        $(this).parent().find('.grid_width').text(title);
                        updateHtmlContent();
                    }                   
               }                
            }
        }
        
    });
    
    //add grid element to Editor div
    $(document).delegate('.custom-layout-cols div[class ^="icon-grid" ]', 'click', function(){
        var dataId = $(this).attr('data-id');    
        var title  = $(this).attr('title'); 
        var editor = $(this).parent().parent().parent().find('.row-content');
        $( gridElement(title, '') ).addClass(dataId).appendTo(editor);
    });
     
        
    //set active class to the current content col
    $(document).delegate('.add-element', 'click', function(){
        $('.elemet-content').removeClass('active-content');
        $(this).parent().parent().parent().find('.elemet-content').addClass('active-content');
    });
    
    //delete-element
    $(document).delegate('.delete-element, .delete-form-element', 'click', function(){
        $(this).parent().parent().parent().remove();
        updateHtmlContent();
    });
    
    //delete-element
    $(document).delegate('.edit-form-element', 'click', function(){
        
        //updateHtmlContent();
    });
   
    //add new row for visual editor
    $(document).delegate('.add-row', 'click', function(event){    
        event.preventDefault();
        //$("#visual-builder-mode .row-block").first().clone().insertAfter("#visual-builder-mode .row-block").last().find('.row-content').html('');
        var firstClone = $("#visual-builder-mode .row-block").first().clone();
        var lastRow = $("#visual-builder-mode .row-block").last();
        var thisRow = $(this).parent().parent();
        firstClone.insertAfter(thisRow).find('.row-content').html('');
        updateHtmlContent();
    });
    
    //clone new row for visual editor
    $(document).delegate('.clone-row', 'click', function(event){  
        event.preventDefault();
        var thisRow = $(this).parent().parent();
        var thisCloneRow = $(this).parent().parent().clone();
        thisCloneRow.insertAfter(thisRow);
        updateHtmlContent();
    });
    
    //Delete row
    $(document).delegate('.delete-row', 'click', function(){
        $(this).parent().parent().remove();
        updateHtmlContent();
    });
    
    //set defoult class to element if element wrap checked
    if ( $('#elmWrap').prop( "checked" ) ){
        $('#elmClass').val( 'form-control');
    }
    
    //is elemnt wrap checked
    $(document).delegate('#elmWrap', 'change', function(){        
        if ($(this).prop('checked')) {
            //blah blah            
            $('#elmClass').val( 'form-control');
        }else{
            // console.log('unchecked');
            $('#elmClass').val('');
        }
    });
    
    //input elements with labels    
    $(document).delegate(".elm-group", 'mouseenter mouseleave', function ( event ) {        
        if (event.type == 'mouseenter') {
            $(this)
                .find('label')
                .append('<span class="elm-group-actions">  \n\
                            <a data-target="#option-input-edit" title="Edit Element" class="glyphicon glyphicon-pencil edit-form-element"></a>\n\
                            <a href="#" title="Delete Element" class="glyphicon glyphicon-trash delete-form-element"></a>\n\
                        </span>');
                        
        } else {
            $(this)
                .find('label .elm-group-actions')
                .remove();
        
        }
    });
    
    //Get Element Options modal content
    function getElmContent( parentSelector ) {
        
        var dataAttr,dataStyle, valAttr, content; 
        var inputAttr = '', styleAttr = '', wrapStart = '', wrapEnd = '';
        var dataType = $( parentSelector + ' .select-type').filterByData('type').val();
        var dataLabel = $( parentSelector + ' input').filterByData('label').val();
        var dataAttrObj = $( parentSelector + ' input').filterByData('attribute');  
        var dataStyleObj = $( parentSelector + ' input').filterByData('style');  
        console.log(dataLabel);
        //input types
        $.each(dataAttrObj, function( index, value ) {
            dataAttr = $(this).attr('data-attribute');
            valAttr = $(this).val();
            if( valAttr != '' ){
                inputAttr += dataAttr + '="'+valAttr+'" ';
            }
        });
        
        $.each(dataStyleObj, function( index, value ) {
            dataAttr = $(this).attr('data-style');
            valAttr = $(this).val();
            if( valAttr != '' ){
                styleAttr += dataAttr + ":"+valAttr + "; ";
            }
        });
        
        //is wrap enabled
        if ( $( parentSelector + ' #elmWrap').prop( "checked" ) ){
            wrapStart = '<div class="form-group elm-group block-elm">';
            wrapEnd  = '</div>';
        } else{
            wrapStart = '<span class="form-group elm-group inline-elm">';
            wrapEnd  = '</span>';
        }
               
        content  = wrapStart;
        content  += '<label>'+dataLabel+'</label>';
        content  += '<input type="'+dataType+'" '+ inputAttr +' style="'+styleAttr+'"  />';
        content  += wrapEnd; 
        
        return content;
    };
    
    //option-input-type inser element
    $('#option-input-types #add-elm, #option-text-block #add-elm').on( 'click', function(){               
         var content = getElmContent('#option-input-types');                
        $('.active-content').append(content);        
        //adding html content to textaea
        updateHtmlContent();
    });
    
     //option-input-type inser element
    $('#option-input-edit #update-elm').on( 'click', function(){               
        var content = getElmContent('#option-input-edit'); 
        console.log(content);
       $('.activeEditableElem').addClass('oldElement');
        $(content).insertBefore('.activeEditableElem');   
        $('.oldElement').remove();   
        
        //adding html content to textaea
        updateHtmlContent();
    });
    
    //option-input-type UPDATE element
    $(document).delegate(".edit-form-element", 'click', function ( event ) {  
        
        $('.elm-group').removeClass('activeEditableElem');
        $(this).parent().parent().parent().addClass('activeEditableElem');
        
        $('#option-input-edit').modal('show');
                
        //getting and setting input values 
        var inputType = $('.activeEditableElem input').attr('type');                
        var inputW = $(".activeEditableElem input").inlineStyle("width");
        var inputH = $('.activeEditableElem input').inlineStyle('height');
        $('#option-input-edit option[value='+inputType+']').attr('selected','selected');
        if ( inputW  ){ $('#option-input-edit #input-styles [data-style="width"]').val(inputW); }
        if ( inputH ){ $('#option-input-edit #input-styles [data-style="height"]').val(inputH); }
        
        
        //getting and setting label values 
        var labelTxt = $('.activeEditableElem label').text();
        var labelW = $('.activeEditableElem label').inlineStyle('width');
        var labelH = $('.activeEditableElem label').inlineStyle('height');
        $('#option-input-edit').find('#label-text').val(labelTxt);
        if ( inputW  ){ $('#option-input-edit #label-styles [data-style="width"]').val(labelW); }
        if ( inputH ){ $('#option-input-edit #label-styles [data-style="height"]').val(labelH); }
        
        //getting and setting val to checkbox Wrap to full Width div
        if( $('.activeEditableElem').hasClass('block-elm') ){
            $('#option-input-edit #elmWrap').prop('checked' , true);
        }
        if( $('.activeEditableElem').hasClass('inline-elm') ){
            $('#option-input-edit #elmWrap').prop('checked' , false);
        }
        
        //getting and setting attribute values
        var elmName = $('.activeEditableElem input').attr('name');
        if ( elmName ){
            $('#option-input-edit #elmName').val(elmName);
        }
        var elmId = $('.activeEditableElem input').attr('id');
        if ( elmId ){
            $('#option-input-edit #elmId').val(elmId);
        }
        var elmClass = $('.activeEditableElem input').attr('class');
        if ( elmClass ){
            $('#option-input-edit #elmClass').val(elmClass);
        }
        var elmPlaceholder = $('.activeEditableElem input').attr('placeholder');
        if ( elmPlaceholder ){
            $('#option-input-edit #elmPlaceholder').val(elmPlaceholder);
        }
        var elmValue = $('.activeEditableElem input').attr('value');
        if ( elmValue ){
            $('#option-input-edit #elmValue').val(elmValue);
        }
        
        
        //Update html content to textaea
        updateHtmlContent();
    });
    
    
     //parse html val from html editor and show visual mode 
    //checking if no data than add empty row
    if ( $('#html-content').val() == ''  ){
         var visualMode = $('#visual-builder-mode')
        $( gridRow() ).appendTo(visualMode);
    } 
    parseHTMLToVisual();
    
    
});

//taking html from visual mode and adding to textarea as a row html
function updateHtmlContent(){
    var htmlContent = '', content= '', thisClasses = '',
            row, rowClasses = '', thisContent = '',
            thisGridWidth='1/1', thisHtmlObj = '';
        var gridRow = $('.row-block');
        var htmlObj = $('.grid-item-wrapper');
        $.each( gridRow, function( index, value ) {
            rowClasses = $( this ).attr('class');
            thisHtmlObj = $(this).find('.grid-item-wrapper');
            htmlContent += '<div class="'+rowClasses+'" >';
            $.each(thisHtmlObj, function( index, value ) {
                thisClasses = $( this ).attr('class');                
                thisGridWidth = $(this).find('.grid_width').text();
                thisContent = $( this ).find('.elemet-content'); 
                htmlContent += '<div data-id="'+thisGridWidth+'" class="'+thisClasses+'" >';
                htmlContent += thisContent[0].innerHTML;
                htmlContent += '</div>';
            });
                       
            htmlContent += '</div>';
        });
        $('#html-content').val( htmlContent );
        $('#json-content').val('jsonObjectHere');
}

//taking html from textarea value and convert it to visual mode
function parseHTMLToVisual(){
    //parseHTML
    var htmlVal = $('#html-content').val();
    if ( htmlVal ) {
        var htmlRow = $.parseHTML( htmlVal );
        var editorContent = '', gridItem, row, thisRowContent;
        var editor = $('.row-content');
        var visualMode = $('#visual-builder-mode');
        var dataId, title, innerHtmlVal, elementClass, gridElm='';
       // console.log(htmlRow);
        $.each( htmlRow, function( i, val ){

            row = $( gridRow() ).appendTo(visualMode);
            thisRowContent = row.find('.row-content');
            gridItem = $(this).find('.grid-item-wrapper');

            $.each( gridItem, function( index, value ){           
                dataId = value.attributes['data-id'];
                title = dataId.nodeValue;
                innerHtmlVal = value.innerHTML;
                elementClass = value.className;
                gridElm = $( gridElement(title, innerHtmlVal) ).addClass(elementClass).appendTo(thisRowContent);

            });

            // console.log(gridElm);

        });
    }
    
   
}

//grid Row element
function gridRow(){
    var gridRow = '<div class="row-block">\n\
                        <div class="row-controls">\n\
                                <div class="custom-layout-cols pull-left">\n\
                                    <div class="icon-grid-1" data-id="grid_one" title="1/1"></div>\n\
                                    <div class="icon-grid-2" data-id="grid_two" title="1/2"></div>\n\
                                    <div class="icon-grid-3" data-id="grid_three" title="1/3"></div>\n\
                                    <div class="icon-grid-2-3" data-id="grid_two_third" title="2/3"></div>\n\
                                    <div class="icon-grid-4" data-id="grid_four" title="1/4"></div>\n\
                                    <div class="icon-grid-3-4" data-id="grid_three_fourth" title="3/4"></div>\n\
                                    <div class="icon-grid-5" data-id="grid_five" title="1/5"></div>\n\
                                    <div class="icon-grid-2-5" data-id="grid_two_fifth" title="2/5"></div>\n\
                                    <div class="icon-grid-3-5" data-id="grid_three_fifth" title="3/5"></div>\n\
                                    <div class="icon-grid-4-5" data-id="grid_four_fifth" title="4/5"></div>\n\
                                    <div class="icon-grid-6" data-id="grid_six" title="1/6"></div>\n\
                                    <div class="icon-grid-5-6" data-id="grid_five_sixth" title="5/6"></div>\n\
                                </div>\n\
                                <i title="Add New Row" class="add-row glyphicon glyphicon-plus"></i>\n\
                                <i title="Duplicate Row" class="clone-row glyphicon glyphicon-duplicate"></i>\n\
                                <i title="Row Options" class="edit-row glyphicon glyphicon-cog"></i>\n\
                                <i title="Delete Row" class="delete-row glyphicon glyphicon-trash"></i>\n\
                            </div>\n\
                        <div class="row-content"></div>\n\
                    </div>';
    return gridRow;
}

//grid element inside row element
function gridElement(title, content){
    
    var gridElm= '<div class="grid-item-wrapper" >\n\
                    <div class="handler">\n\
                        <a class="decrease-width change-width" href="javascript:void(0);" title="Decrease width"><i class="glyphicon glyphicon-plus"></i></a>\n\
                        <a class="increase-width change-width" href="javascript:void(0);" title="Increase width"><i class="glyphicon glyphicon-minus"></i></a> \n\
                        <span class="grid_width">'+title+'</span>\n\
                        <div id="simple_option" class="columns-options builder-element">\n\
                            <a data-toggle="modal" data-target="#grid-elements-modal" class="add-element" title="Add Element"><i class="glyphicon glyphicon-import"></i></a>\n\
                            <a class="edit-element " href="javascript:void(0);" title="Edit Element"><i class="glyphicon glyphicon-edit"></i></a>\n\
                            <a class="clone-element" href="javascript:void(0);" title="Clone Element"><i class="glyphicon glyphicon-duplicate"></i></a>\n\
                            <a class="delete-element" href="javascript:void(0);" title="Delete Element"><i class="glyphicon glyphicon-trash"></i></a>\n\
                        </div>\n\
                    </div>\n\
                    <div class="innerElement elemet-content">'+content+'</div>\n\
                  </div>';
    
    return gridElm;
}