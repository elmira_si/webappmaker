<?php require_once 'header.php'; ?>


<div class="container-fluid">
      <div class="row">
        
        <?php //require_once 'left-sidebar.php'; ?>
        
        <?php
            if ( !isset( $_GET['url'] ) ){
                require_once 'partials/_overview.php';
            } else {                
                require_once 'partials/_'.$_GET['url'].'.php';     
            }
          
         ?>
       
    </div>
</div>


<?php require_once 'footer.php'; ?>