<?php
    
    
?>
<div class="col-sm-3 col-md-2 sidebar">
    <ul class="nav nav-sidebar">
        <li class="<?php echo isActvMenu('overview'); ?>">
            <a href="?url=overview">Overview</a>   
        </li>
        <li class="<?php echo isActvMenu('pages'); ?>">
            <a href="?url=pages">Pages</a>
            <ul>
                <li class="<?php echo isActvMenu('pages'); ?>"><a href="?url=pages">All Pages</a></li>
                <li class="<?php echo isActvMenu('new-pages'); ?>"><a href="?url=new-page">New Page</a></li>
            </ul>
        </li>
        <li class="<?php echo isActvMenu('forms'); ?>">
            <a href="?url=forms">Forms</a>
            <ul>
                <li class="<?php echo isActvMenu('forms'); ?>"><a href="?url=forms">All Forms</a></li>
                <li class="<?php echo isActvMenu('new-form'); ?>"><a href="?url=new-form">New Form</a></li>
            </ul>
        </li>
        <li class="<?php echo isActvMenu('objects'); ?>">
              <a href="?url=objects">Objects</a>
              <ul>
                  <li class="<?php echo isActvMenu('objects'); ?>"><a href="?url=objects">All Objects</a></li>
                  <li class="<?php echo isActvMenu('new-object'); ?>"><a href="?url=new-object">New Object</a></li>
              </ul>
        </li>
        <li class="<?php echo isActvMenu('menues'); ?>">
            <a href="?url=menues">Menues</a>
        </li>
    </ul>

    
  </div>