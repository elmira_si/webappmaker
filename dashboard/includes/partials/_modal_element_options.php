<!--Input Elemts modal START-->
<div class="modal fade" id="option-input-types" tabindex="-2" role="dialog" data-dismiss="modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title " id="myModalLabel"><i class="glyphicon glyphicon-cog"></i> Element Options</h4>
            </div>
            <div class="modal-body">

                <div class="row">

                    <div class="col-md-6">
                        
                        <div class="form-group">
                            <label for="input-type">Input Type: </label>
                            <select class="form-control select-type" data-type="input-type">                                
                                <option value="text">Text</option>
                                <option value="password">Password</option>                                
                                <option value="submit">Submit</option>                                
                                <option value="radio">Radio</option>    
                                <option value="checkbox">Checkbox</option>    
                                <option value="button">Button</option>    
                                <option value="hidden">Hidden</option>    

                                <optgroup label="HTML5 Input Types">
                                  <option value="color">Color</option>
                                  <option value="date">Date</option>
                                  <option value="datetime">Datetime</option>
                                  <option value="datetime-local">Datetime-local</option>
                                  <option value="email">Email</option>
                                  <option value="month">Month</option>
                                  <option value="number">Number</option>
                                  <option value="range">Range</option>
                                  <option value="search">Search</option>
                                  <option value="tel">Tel</option>
                                  <option value="time">Time</option>
                                  <option value="url">Url</option>
                                  <option value="week">Week</option>
                                </optgroup>
                            </select>
                        </div> 
                        <div  class="form-inline whstyles">                            
                            <input data-style="width" type="text" class="form-control width" placeholder="Input Width" data-style="width" />    
                            <input data-style="height" type="text" class="form-control height" placeholder="Input Height" data-style="height" />
                        </div>
                       
                        <div class="form-group">    
                            <label>Label: </label>
                            <input data-label="label" type="text" class="form-control" placeholder="Label Text" />                            
                        </div>
                        <div  class="form-inline whstyles">                            
                            <input data-style="width" type="text" class="form-control width" placeholder="Label Width" />    
                            <input data-style="height" type="text" class="form-control height" placeholder="Label Height" />
                        </div>
                        <div  class="form-group">                             
                            <input id="elmWrap" data-wrap="div" type="checkbox" checked="checked" />
                            <strong> Wrap to full width div </strong>
                        </div>
                        <div class="form-group" >
                            <label>Plain JavaScript Code: </label>
                            <textarea id="elmJsCode" data-js-code="js-code" class="form-control" placeholder="JavaScript Or Jquery Code"></textarea>
                        </div>
                        
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">    
                            <label>Name: </label>
                            <input id="elmName" data-attribute="name" type="text" class="form-control" placeholder="name-attribute" />
                        </div>
                        
                        <div class="form-group">    
                            <label>Id: </label>
                            <input id="elmId" data-attribute="id" type="text" class="form-control" placeholder="id-attribute" />
                        </div>
                        
                        <div class="form-group">    
                            <label>Classes: </label>
                            <input id="elmClass" data-attribute="class" type="text" class="form-control" placeholder="class1 class2 class3 " />
                        </div>

                        <div class="form-group">
                            <label>Placeholder: </label>
                            <input id="elmPlaceholder" data-attribute="placeholder" type="text" class="form-control" placeholder="Enter Placeholder Text" />
                        </div>

                        <div class="form-group">
                            <label>Value: </label>
                            <input id="elmValue" data-attribute="value" type="text" class="form-control" placeholder="Enter Input Value" />
                        </div>
                    </div>
                </div>
            </div>            
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="add-elm">Add Element</button>
            </div>            
        </div>
    </div>
</div>
<!--Input Elemts modal END-->

<!--Form insert modal START-->
 <?php    
    $db = Db::getInstance();
    $table = "forms";         
    $forms = $db->selectAll($table, '1', 5);          
?>
<div class="modal fade" id="option-forms" tabindex="-2" role="dialog" data-dismiss="modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title " id="myModalLabel"><i class="glyphicon glyphicon-cog"></i> Form Element Options</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">   
                        <div class="form-group">    
                            <label>Form Heading: </label>
                            <input data-label="label" type="text" class="form-control" placeholder="Form Heading text..." />                            
                        </div>
                        <div class="form-group">
                            <label for="input-type">Select Form: </label>
                            <select id="select-form" class="form-control" data-type="form"> 
                                <?php 
                                /* Select queries return a resultset */
                               if($forms){
                                   // Cycle through results
                                  while ($row = $forms->fetch_object()){
                               ?>
                                <option value="<?php echo $row->ID; ?>">
                                    <?php echo '#'.$row->ID.' Title'. $row->title; ?>
                                </option>
                                <?php }
                                    // Free result set
                                    $forms->close();
                                }
                                ?>
                            </select>
                        </div>
                </div>
            </div>            
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="add-form-elm">Add Form</button>
            </div>
            
        </div>
    </div>
</div>
</div>
<!--Form insert modal START-->

<!--Form insert modal START-->
<div class="modal fade" id="option-text-block" tabindex="-2" role="dialog" data-dismiss="modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title " id="myModalLabel"><i class="glyphicon glyphicon-cog"></i> Text Block Element Options</h4>
            </div>
            <div class="modal-body">
                <div class="row">

                    <div class="col-md-6">
                        
                        <div class="">
                            <input type="hidden" class="select-type"  data-type="textarea" value="textarea" />
                            <label for="input-type">Sizes Width/Height: </label>                           
                        </div>
                        <div  class="form-inline whstyles">                            
                            <input data-style="width" type="text" class="form-control width" placeholder="Textarea Width" />    
                            <input data-style="height" type="text" class="form-control height" placeholder="Textarea Height" />
                        </div>
                        <div  class="form-inline whstyles">                            
                            <input data-style="cols" type="text" class="form-control width" placeholder="Textarea Cols" />    
                            <input data-style="rows" type="text" class="form-control height" placeholder="Textarea Rows" />
                        </div>
                       
                        <div class="form-group">    
                            <label>Label: </label>
                            <input data-label="label" type="text" class="form-control" placeholder="Label Text" />                            
                        </div>
                        <div  class="form-inline whstyles">                            
                            <input data-style="width" type="text" class="form-control width" placeholder="Label Width" />    
                            <input data-style="height" type="text" class="form-control height" placeholder="Label Height" />
                        </div>
                        <div  class="form-group">                             
                            <input id="elmWrap" data-wrap="div" type="checkbox" checked="checked" />
                            <strong> Wrap to full width div </strong>
                        </div>
                        <div class="form-group" >
                            <label>Plain JavaScript Code: </label>
                            <textarea id="elmJsCode" data-js-code="js-code" class="form-control" placeholder="JavaScript Or Jquery Code"></textarea>
                        </div>
                        
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">    
                            <label>Name: </label>
                            <input id="elmName" data-attribute="name" type="text" class="form-control" placeholder="name-attribute" />
                        </div>
                        
                        <div class="form-group">    
                            <label>Id: </label>
                            <input id="elmId" data-attribute="id" type="text" class="form-control" placeholder="id-attribute" />
                        </div>
                        
                        <div class="form-group">    
                            <label>Classes: </label>
                            <input id="elmClass" data-attribute="class" type="text" class="form-control" placeholder="class1 class2 class3 " />
                        </div>

                        <div class="form-group">
                            <label>Placeholder: </label>
                            <input id="elmPlaceholder" data-attribute="placeholder" type="text" class="form-control" placeholder="Enter Placeholder Text" />
                        </div>

                        <div class="form-group">
                            <label>Value: </label>
                            <input id="elmValue" data-attribute="value" type="text" class="form-control" placeholder="Enter Input Value" />
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">    
                            <label>Custom Attributes: </label>
                            <input id="elmAttr" data-attribute="custom" type="text" class="form-control" placeholder="data-id='block' data-val='value' " />
                        </div>
                    </div>
                </div>
            </div>            
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="add-elm">Add Text BOx</button>
            </div>
            
        </div>
    </div>
</div>
</div>
<!--Form insert modal START-->



<!--UPDATE MODALS-->
<!--Input Elemts modal START-->
<div class="modal fade" id="option-input-edit" tabindex="-2" role="dialog" data-dismiss="modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title " id="myModalLabel"><i class="glyphicon glyphicon-cog"></i> Edit Element Options</h4>
            </div>
            <div class="modal-body">

                <div class="row">

                    <div class="col-md-6">
                        
                        <div class="form-group">
                            <label for="input-type">Input Type: </label>
                            <select class="form-control" data-type="input-type">                                
                                <option value="text">Text</option>
                                <option value="password">Password</option>                                
                                <option value="submit">Submit</option>                                
                                <option value="radio">Radio</option>    
                                <option value="checkbox">Checkbox</option>    
                                <option value="button">Button</option>    
                                <option value="hidden">Hidden</option>    

                                <optgroup label="HTML5 Input Types">
                                  <option value="color">Color</option>
                                  <option value="date">Date</option>
                                  <option value="datetime">Datetime</option>
                                  <option value="datetime-local">Datetime-local</option>
                                  <option value="email">Email</option>
                                  <option value="month">Month</option>
                                  <option value="number">Number</option>
                                  <option value="range">Range</option>
                                  <option value="search">Search</option>
                                  <option value="tel">Tel</option>
                                  <option value="time">Time</option>
                                  <option value="url">Url</option>
                                  <option value="week">Week</option>
                                </optgroup>
                            </select>
                        </div> 
                        <div id="input-styles" class="form-inline whstyles">                            
                            <input data-style="width" type="text" class="form-control width" placeholder="Input Width" />    
                            <input data-style="height" type="text" class="form-control height" placeholder="Input Height" />
                        </div>
                       
                        <div class="form-group">    
                            <label>Label: </label>
                            <input id="label-text" data-label="label" type="text" class="form-control" placeholder="Label Text" />                            
                        </div>
                        <div id="label-styles" class="form-inline whstyles">                            
                            <input data-style="width" type="text" class="form-control width" placeholder="Label Width" />    
                            <input data-style="height" type="text" class="form-control height" placeholder="Label Height" />
                        </div>
                        <div  class="form-group">                             
                            <input id="elmWrap" data-wrap="div" type="checkbox" checked="checked" />
                            <strong> Wrap to full width div </strong>
                        </div>
<!--                        <div class="form-group" >
                            <label>Plain JavaScript Code: </label>
                            <textarea id="elmJsCode" data-js-code="js-code" class="form-control" placeholder="JavaScript Or Jquery Code"></textarea>
                        </div>-->
                        
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">    
                            <label>Name: </label>
                            <input id="elmName" data-attribute="name" type="text" class="form-control" placeholder="name-attribute" />
                        </div>
                        
                        <div class="form-group">    
                            <label>Id: </label>
                            <input id="elmId" data-attribute="id" type="text" class="form-control" placeholder="id-attribute" />
                        </div>
                        
                        <div class="form-group">    
                            <label>Classes: </label>
                            <input id="elmClass" data-attribute="class" type="text" class="form-control" placeholder="class1 class2 class3 " />
                        </div>

                        <div class="form-group">
                            <label>Placeholder: </label>
                            <input id="elmPlaceholder" data-attribute="placeholder" type="text" class="form-control" placeholder="Enter Placeholder Text" />
                        </div>

                        <div class="form-group">
                            <label>Value: </label>
                            <input id="elmValue" data-attribute="value" type="text" class="form-control" placeholder="Enter Input Value" />
                        </div>
                    </div>

                </div>


            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="update-elm">Update Element</button>
            </div>
            
        </div>
    </div>
</div>
<!--Input Elemts modal END-->
    

