<?php
    
    if( isset($_POST) && !empty($_POST) ){
        
        extract($_POST);
        
        $date = date('Y-m-d H:i:s', strtotime( 'now' ));    
        
        $data = array(
            'title' => $title,
            'htmlObj' => $htmlObj,
            'jsonObj' => $jsonObj,
            'order' => '1',
            'author' => get_author_id(),
            'created_at' => $date,
            'last_modified' => $date
        );
        
        $action = new Actions();
        $action->saveForm($data);
    }
?>

<div class="col-md-12 main">
    <h1 class="page-header">New Form</h1>
    
    <div class="row">
        <div class="col-md-12 new-form">
            
            <form action="" method="post">
                <div class="form-group">
                    <label for="title">Form Title*</label>
                    <input name="title" id="form-title" class="form-control" type="text" value="" />
                </div>
                
                <div class="form-group">
                    <div id="tabs">
                        <ul>
                            <li class="pull-right"><a href="#form-option-mode">Form Options</a></li>
                            <li class="pull-right"><a href="#html-object-mode">Html Editor Mode</a></li>
                            <li class="pull-right ui-tabs-active "><a href="#visual-builder-mode">Visual Builder Mode</a></li>                        
                        </ul>

                        <div id="visual-builder-mode" >
                            
<!--                            <div class="row-block">
                                <div class="row-controls">                                                                        
                                    <div class="custom-layout-cols pull-left">
                                        <div id="icon-grid-full" data-id="grid_full" title="Full Width Container"></div>
                                        <div class="icon-grid-1" data-id="grid_one" title="1/1"></div>
                                        <div class="icon-grid-2" data-id="grid_two" title="1/2"></div>
                                        <div class="icon-grid-3" data-id="grid_three" title="1/3"></div>
                                        <div class="icon-grid-2-3" data-id="grid_two_third" title="2/3"></div>
                                        <div class="icon-grid-4" data-id="grid_four" title="1/4"></div>
                                        <div class="icon-grid-3-4" data-id="grid_three_fourth" title="3/4"></div>
                                        <div class="icon-grid-5" data-id="grid_five" title="1/5"></div>
                                        <div class="icon-grid-2-5" data-id="grid_two_fifth" title="2/5"></div>
                                        <div class="icon-grid-3-5" data-id="grid_three_fifth" title="3/5"></div>
                                        <div class="icon-grid-4-5" data-id="grid_four_fifth" title="4/5"></div>
                                        <div class="icon-grid-6" data-id="grid_six" title="1/6"></div>
                                        <div class="icon-grid-5-6" data-id="grid_five_sixth" title="5/6"></div>                                
                                    </div>
                                    
                                    <i title="Add New Row" class="add-row glyphicon glyphicon-plus"></i>
                                    <i title="Duplicate Row" class="clone-row glyphicon glyphicon-duplicate"></i>
                                    <i title="Row Options" class="edit-row glyphicon glyphicon-cog"></i>
                                    <i title="Delete Row" class="delete-row glyphicon glyphicon-trash"></i>                                    
                                </div>
                                <div class="row-content"></div> Visual Buider editor/ dont'remove  
                            </div>-->
                                             

                        </div>
                        
                        <div id="html-object-mode">  
                            <div class="form-group">
                                <label id="htmlObj">Row Html Text</label>
                                <textarea name="htmlObj" rows="10" id="html-content" class="form-control"></textarea> <br/><br/>  
                                <label>Row Json Object</label>
                                <textarea name="jsonObj" rows="10" id="json-content" class="form-control"></textarea>     
                            </div>
                        </div>
                        
                        <div id="form-option-mode">form options here</div>

                    </div>                    
                </div>
                
               <div class="form-group">
                    <div class="col-">
                        <button name="send" type="submit" class="btn btn-lg btn-success pull-right">Save Form</button>
                    </div>
                </div>
                
                <input type="hidden" name="action" value="saveForm" />
            </form>
        </div>
        
        
    </div>
        
        
    
    
</div>


<!-- Elemts Modal -->
<?php require_once '_modal_form_elemnts.php';?>
