<?php
    $action = new Actions();

    $users = $action->getFields('users',array('user_id', 'fname', 'lname'));
    $pages = $action->getFields('pages',array('ID', 'title','status'));
    
    
    if( isset($_POST) && !empty($_POST) ){
        
        extract($_POST);
        
        $date = date('Y-m-d H:i:s', strtotime( 'now' ));    
        
        $data = array(
            'title' => $title,
            'htmlObj' => $htmlObj,
            'jsonObj' => $jsonObj,
            'status' => $status,
            'order' => $order,
            'author' => $author,
            'parent' => $parent,
            'created_at' => $date,
            'last_modified' => $date
        );
        
        $action = new Actions();
        $pageId = $action->saveRow( 'pages', $data );
        if( $pageId ){
            //redirect edit page of new crated page
        }
    }
?>

<div class="col-md-12 main">
    <h1 class="page-header">New Page</h1>
    
    
        <form action="" method="POST">
            <div class="form col-md-10">
                <div class="form-group">
                    <label for="title">Title</label>
                    <input name="title" type="text" class="form-control" id="title" placeholder="Page Title">
                </div>            
               <div class="form-group">
                    <div id="tabs">
                        <ul>
                            <li class="pull-right"><a href="#html-object-mode">Html Editor Mode</a></li>
                            <li class="pull-right ui-tabs-active "><a href="#visual-builder-mode">Visual Builder Mode</a></li>                        
                        </ul>

                        <div id="visual-builder-mode" >                                            

                        </div>
                        
                        <div id="html-object-mode">  
                            <div class="form-group">
                                <label id="htmlObj">Row Html Text</label>
                                <textarea name="htmlObj" rows="10" id="html-content" class="form-control"></textarea> <br/><br/>  
                                <label>Row Json Object</label>
                                <textarea name="jsonObj" rows="10" id="json-content" class="form-control"></textarea>     
                            </div>
                        </div>

                    </div>                    
                </div>   
            </div>
            <div class="form col-md-2">
                <div class="form-group">
                    <label for="title">Status</label>
                    <select name="status" class="form-control">
                        <option value="Pablished" > Select Status </option>
                        <?php foreach ($pages as $key => $val): ?>
                            <option value="<?php echo $val['status']; ?>" ><?php echo $val['status']; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="Order">Order</label>
                    <input type="number" name="order" class="form-control" id="Order" placeholder="Page Order">
                </div>
                <div class="form-group">
                    <label for="author">Author</label>
                    <select name=" author" class="form-control">
                        <option value=""> Select Author </option>
                        <?php foreach ($users as $key => $val): ?>
                            <option value="<?php echo $val['user_id']; ?>" ><?php echo $val['fname'].' '.$val['lname']; ?></option>
                       <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="parent">Parent</label>
                    <select name="parent" class="form-control">
                        <option value="0"> Select Parent </option>
                        <?php foreach ($pages as $key => $val): ?>
                            <option value="<?php echo $val['title']; ?>" ><?php echo $val['title']; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group text-center">
                    <button id='preview' data-toggle="modal" data-target="#form-preview-modal" name="preview" type="button" class="btn btn-info">Preview</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
            
        </form>
        
</div>
    
    
<!-- Elemts Modal -->
<?php require_once '_modal_preview.php';?>
<?php require_once '_modal_page_elemnts.php';?>