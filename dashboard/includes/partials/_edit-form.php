<?php

$formId = $_GET['id'];

$action = new Actions();
$data = $action->getFormData($formId);

 if( isset($_POST) && !empty($_POST) ){
        
        extract($_POST);
        
        $date = date('Y-m-d H:i:s', strtotime( 'now' ));    
        
        $data = array(
            'title' => $title,
            'htmlObj' => $htmlObj,
            'jsonObj' => $jsonObj,
            'order' => '1',
            'last_modified' => $date
        );
        
        $action = new Actions();
        $action->updateForm( $data, $formId );
    }

?>
<div class="col-md-12 main">
    <h1 class="page-header">Edit Form</h1>
    
    <div class="row">
        <div class="col-md-12 new-form">
            
            <form action="" method="post">
                <div class="form-group">
                    <label for="title">Form Title*</label>
                    <input name="title" id="form-title" class="form-control" type="text" value="<?php echo $data['title']; ?>" />
                </div>
                
                <div class="form-group">
                    <div id="tabs">
                        <ul>
                            <li class="pull-right"><a href="#form-option-mode">Form Options</a></li>
                            <li class="pull-right"><a href="#html-object-mode">Objects</a></li>
                            <li class="pull-right ui-tabs-active "><a href="#visual-builder-mode">Visual Builder Mode</a></li>                        
                        </ul>

                        <div id="visual-builder-mode" ></div>
                        
                        <div id="html-object-mode">  
                            <div class="form-group">
                                <label id="htmlObj">Row Html Text</label>
                                <textarea name="htmlObj" rows="10" id="html-content" class="form-control"><?php echo $data['htmlObj']; ?></textarea> <br/><br/>  
                                <label>Row Json Object</label>
                                <textarea name="jsonObj" rows="10" id="json-content" class="form-control"><?php echo $data['jsonObj']; ?></textarea>     
                            </div>
                        </div>
                        
                        <div id="form-option-mode">form options here</div>

                    </div>                    
                </div>
                
               <div class="form-group">
                    <div class="text-right">                        
                        <button id='preview' data-toggle="modal" data-target="#form-preview-modal" name="preview" type="button" class="btn btn-lg btn-primary">Preview</button>
                        <button name="send" type="submit" class="btn btn-lg btn-success">Update Form</button>
                    </div>
                </div>
                
                <input type="hidden" name="action" value="saveForm" />
            </form>
        </div>
        
        
    </div>
    
</div>


<!-- Elemts Modal -->
<?php require_once '_modal_preview.php';?>
<?php require_once '_modal_form_elemnts.php';?>