<?php
    
        $db = Db::getInstance();
        $table = "forms"; 
        
        $result = $db->selectAll($table);               
       
    ?>
    

<div class="col-md-12 main">
    <h1 class="page-header">All Forms</h1>
    <div class="table-responsive">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>#</th>
            <th>Title</th>
            <th>Order</th>
            <th>Author</th>
            <th>Created</th>
            <th>Last Modified</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
            <?php 
                 /* Select queries return a resultset */
                if($result){
                    // Cycle through results
                   while ($row = $result->fetch_object()){
                ?>            
                <tr>
                    <td><?php echo $row->ID; ?></td>
                    <td><?php echo $row->title; ?></td>
                    <td><?php echo $row->order; ?></td>
                    <td><?php echo $row->author; ?></td>
                    <td><?php echo $row->created_at; ?></td>
                    <td><?php echo $row->last_modified; ?></td>
                    <td>
                        <a href="?url=edit-form&id=<?php echo $row->ID; ?>" title="Edit Form" class="glyphicon glyphicon-pencil"></a>  
                        <a href="#" title="Delete Form" class="glyphicon glyphicon-trash"></a>
                    </td>
                </tr>
            <?php }
                   // Free result set
                   $result->close();
               }
            ?>
          
          

        </tbody>
      </table>
    </div>
    
</div>