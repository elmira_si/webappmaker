<div class="col-md-12 main">
    <h1 class="page-header">Dashboard</h1>

<!--    <div class="row placeholders">
      <div class="col-xs-6 col-sm-3 placeholder">
        <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
        <h4>Label</h4>
        <span class="text-muted">Something else</span>
      </div>
      <div class="col-xs-6 col-sm-3 placeholder">
        <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
        <h4>Label</h4>
        <span class="text-muted">Something else</span>
      </div>
      <div class="col-xs-6 col-sm-3 placeholder">
        <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
        <h4>Label</h4>
        <span class="text-muted">Something else</span>
      </div>
      <div class="col-xs-6 col-sm-3 placeholder">
        <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
        <h4>Label</h4>
        <span class="text-muted">Something else</span>
      </div>
    </div>-->

    <?php    
        $db = Db::getInstance();
        $table = "forms";         
        $result = $db->selectAll($table, '1', 5);          
    ?>
    <h2 class="sub-header">Forms</h2>
    <div class="table-responsive">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>#</th>
            <th>Title</th>
            <th>Order</th>
            <th>Author</th>
            <th>Created</th>
            <th>Last Modified</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
            <?php 
                 /* Select queries return a resultset */
                if($result){
                    // Cycle through results
                   while ($row = $result->fetch_object()){
                ?>            
                <tr>
                    <td><?php echo $row->ID; ?></td>
                    <td><?php echo $row->title; ?></td>
                    <td><?php echo $row->order; ?></td>
                    <td><?php echo $row->author; ?></td>
                    <td><?php echo $row->created_at; ?></td>
                    <td><?php echo $row->last_modified; ?></td>
                    <td>
                        <a href="?url=edit-form&id=<?php echo $row->ID; ?>" title="Edit Form" class="glyphicon glyphicon-pencil"></a>  
                        <a href="#" title="Delete Form" class="glyphicon glyphicon-trash"></a>
                    </td>
                </tr>
            <?php }
                   // Free result set
                   $result->close();
               }
            ?>
          
          

        </tbody>
      </table>
    </div>



 <?php    
        $db = Db::getInstance();
        $table = "pages";         
        $result = $db->selectAll($table, '1', 5);          
    ?>
    <h2 class="sub-header">Pages</h2>
    <div class="table-responsive">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>#</th>
            <th>Title</th>
            <th>Status</th>
            <th>Order</th>
            <th>Author</th>
            <th>Parent</th>
            <th>Last Modified</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
            <?php 
                 /* Select queries return a resultset */
                if($result){
                    // Cycle through results
                   while ($row = $result->fetch_object()){
                ?>            
                <tr>
                    <td><?php echo $row->ID; ?></td>
                    <td><?php echo $row->title; ?></td>
                    <td><?php echo $row->status; ?></td>
                    <td><?php echo $row->order; ?></td>
                    <td><?php echo $row->author; ?></td>
                    <td><?php echo $row->parent; ?></td>
                    <td><?php echo $row->created_at; ?></td>
                    <td><?php echo $row->last_modified; ?></td>
                    <td>
                        <a href="?url=edit-page&id=<?php echo $row->ID; ?>" title="Edit Form" class="glyphicon glyphicon-pencil"></a>  
                        <a href="#" title="Delete Form" class="glyphicon glyphicon-trash"></a>
                    </td>
                </tr>
            <?php }
                   // Free result set
                   $result->close();
               }
            ?>
        </tbody>
      </table>
    </div>

    
</div>