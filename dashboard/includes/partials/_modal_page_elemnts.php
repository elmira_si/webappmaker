<div class="modal fade" id="grid-elements-modal" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Elements</h4>
            </div>
            <div class="modal-body">

                <div class="row">

                    <div class="elm_item" data-toggle="modal" data-target="#option-forms"> 
                        <div class="elm_icon forms_icon"></div>
                        <div class="elm_txt">
                            <div class="title">Forms</div>
                            <div class="desc">Insert Form to current page.</div>
                        </div>
                    </div>
                    
                    <div class="elm_item" data-toggle="modal" data-target="#option-input-types"> 
                        <div class="elm_icon forms_icon"></div>
                        <div class="elm_txt">
                            <div class="title">Input Types</div>
                            <div class="desc">Test,Radio,Number,Phone...</div>
                        </div>
                    </div>
                    
                    <div class="elm_item" data-toggle="modal" data-target="#option-text-block">
                        <div class="elm_icon textbox_icon"></div>
                        <div class="elm_txt">
                            <div class="title">Text Block</div>
                            <div class="desc">A block of text with editor</div>
                        </div>
                    </div>
                    <div class="elm_item">
                        <div class="elm_icon html_icon"></div>
                        <div class="elm_txt">
                            <div class="title">HTML</div>
                            <div class="desc">Custom Html block</div>
                        </div>
                    </div>
                    <div class="elm_item">
                        <div class="elm_icon js_icon"></div>
                        <div class="elm_txt">
                            <div class="title">JavaScript</div>
                            <div class="desc">Custom JS code block</div>
                        </div>
                    </div>

                </div>


            </div>                          
        </div>
    </div>
</div>

<?php require_once '_modal_element_options.php';?>