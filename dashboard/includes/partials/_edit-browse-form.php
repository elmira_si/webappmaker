
<script>
    //$.jgrid.defaults.width = 780;
    $.jgrid.defaults.responsive = true;
    $.jgrid.defaults.styleUI = 'Bootstrap';
</script>



<div class="col-md-12">
    <h1>BF</h1>
    <input class="btn btn-default" type="button" value="Edit in Batch Mode" onclick="startEdit()" />
    <input class="btn btn-default" type="button" value="Save All Rows" onclick="saveRows()" />

    <br /><br />

    <table id="jqGrid"></table>
    <div id="jqGridPager"></div>
</div>
<script type="text/javascript">

    $(document).ready(function () {
        $("#jqGrid").jqGrid({
            url: 'data.json',
            editurl: 'clientArray',
            datatype: "json",
            colModel: [
                {
                    label: "Employee ID",
                    name: 'EmployeeID',
                    width: 75
                },
                {
                    label: "First Name",
                    name: 'FirstName',
                    width: 140,
                    editable: true // must set editable to true if you want to make the field editable
                },
                {
                    label: "Last Name",
                    name: 'LastName',
                    width: 100,
                    editable: true
                },
                {
                    label: "City",
                    name: 'City',
                    width: 120,
                    editable: true
                }
            ],
            sortname: 'EmployeeID',
            loadonce: true,
            viewrecords: true,
            width: 780,
            height: 200,
            rowNum: 5,
            pager: "#jqGridPager"
        });
    });

    function startEdit() {
        var grid = $("#jqGrid");
        var ids = grid.jqGrid('getDataIDs');

        for (var i = 0; i < ids.length; i++) {
            grid.jqGrid('editRow', ids[i]);
        }
    }
    ;

    function saveRows() {
        var grid = $("#jqGrid");
        var ids = grid.jqGrid('getDataIDs');

        for (var i = 0; i < ids.length; i++) {
            grid.jqGrid('saveRow', ids[i]);
        }
    }

</script>