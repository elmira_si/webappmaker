<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>WebAppMaker Dashboard</title>

    <!-- Bootstrap -->
    <link href="../common/libs/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../common/libs/jquery-ui/css/base/jquery-ui-1.10.4.custom.css" rel="stylesheet">
    <link href="../dashboard/css/dashboard.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="../common/libs/jquery-ui/js/jquery-1.10.2.js"></script>
    <script src="../common/libs/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>    
    <script src="../common/libs/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
    
    
    <!-- We support more than 40 localizations -->
    <script type="text/ecmascript" src="../common/libs/Guriddo_jqGrid/js/trirand/i18n/grid.locale-en.js"></script>
    <!-- This is the Javascript file of jqGrid -->   
    <script type="text/ecmascript" src="../common/libs/Guriddo_jqGrid/js/trirand/jquery.jqGrid.min.js"></script>
    <!-- A link to a Boostrap  and jqGrid Bootstrap CSS siles-->
    <link rel="stylesheet" type="text/css" media="screen" href="../common/libs/Guriddo_jqGrid/css/trirand/ui.jqgrid-bootstrap.css" />



   
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"><?php echo $config['app_name']; ?></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            
            <div class="nav navbar-nav navbar-left" style="padding:10px 0 0 60px">            
                <div class="dropdown pull-left">
                    <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">NEW <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                      <!-- Add header here -->
                      <li><a href="#"><li class="<?php echo isActvMenu('new-pages'); ?>"><a href="?url=new-page">New Page</a></li>
                      <li><a href="#"><li class="<?php echo isActvMenu('new-form'); ?>"><a href="?url=new-form">New Edit Form</a></li>
                      <li><a href="#"><li class="<?php echo isActvMenu('new-browse-form'); ?>"><a href="?url=new-browse-form">New Browse Form</a></li>
                    </ul>
                </div>  
                <div class="dropdown pull-left" style="padding-left: 10px;">
                    <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">Wizard <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                      <!-- Add header here -->
                      <li><a href="#"><li class="<?php echo isActvMenu('new-pages'); ?>"><a href="?url=form-wizard">Form Wizard</a></li>
                    </ul>
                </div>  
                <div class="dropdown pull-left" style="padding-left: 10px;">
                    <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">View <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                      <!-- Add header here -->
                      <li class="<?php echo isActvMenu('pages'); ?>"><a href="?url=pages">All Pages</a></li>
                      <li class="<?php echo isActvMenu('forms'); ?>"><a href="?url=forms">All Forms</a></li>
                    </ul>
                </div>  
                <div class="dropdown pull-left" style="padding-left: 10px;">
                    <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">FTree <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                      <!-- Add header here -->
                      <li class="<?php echo isActvMenu('pages'); ?>"><a href="?url=new-tree">New Tree</a></li>
                    </ul>
                </div>  
            </div>
            
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Settings</a></li>
            <li><a href="#">Profile</a></li>
            <li><a href="#">Help</a></li>
          </ul>
          <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Search...">
          </form>
        </div>
      </div>
    </nav>