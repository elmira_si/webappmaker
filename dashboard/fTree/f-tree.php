<html>
    <head>
        
        <script src="../../../common/libs/jquery-ui/js/jquery-1.10.2.js"></script>
        <script src="../../../common/libs/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>    
        <script src="../../../common/libs/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
        <!--Family tree scripts-->
        <script src="f-tree.js"></script>
        <link href="styles.css" rel="stylesheet" />
    </head>
    <body >
        
        <!--<button class="saveChanges">Save Changes</button>-->

        <div class="drop-container"></div>
        
            <div id="1" data-row='1' class="node male">
                <p >
                    Mother<br/>
                    MG 11<br/>
                    M694V/M680I<br/>
                </p>
            </div>
            <div id="2" data-row='1' class="node female">
                <p >
                    Father<br/>
                    MG 12<br/>
                    M694V/M680I<br/>
                </p>                
            </div>
            <div id="3" data-row='2' class="node unknown">
                <p >
                    Name Surname<br/>
                    MG 15<br/>
                    M694V/M680I<br/>
                </p>
            </div>


        <div style="height: 500px; width: 1px;"></div>


        
    </body>
</html>

