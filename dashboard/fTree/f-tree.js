

// A $( document ).ready() block.
jQuery(document).ready(function($) {
    
    var selectedClass = 'ui-state-highlight',
        clickDelay = 600,     // click time (milliseconds)
        lastClick, diffClick; // timestamps
        
    $( ".node" )
        // Script to deferentiate a click from a mousedown for drag event
        /*.bind('mousedown mouseup', function(e){
            if (e.type=="mousedown") {
                lastClick = e.timeStamp; // get mousedown time
            } else {
                diffClick = e.timeStamp - lastClick;
                if ( diffClick < clickDelay ) {
                    // add selected class to group draggable objects
                    $(this).toggleClass(selectedClass);
                }
            }
        })*/
        .draggable({
            
            /*revertDuration: 10, // grouped items animate separately, so leave this number low
            containment: '.node',
            start: function(e, ui) {
                ui.helper.addClass(selectedClass);
            },
            stop: function(e, ui) {
                // reset group positions
                $('.' + selectedClass).css({ top:0, left:0 });
            },
            drag: function(e, ui) {
                // set selected group position to main dragged object
                // this works because the position is relative to the starting position
                $('.' + selectedClass).css({
                    top : ui.position.top,
                    left: ui.position.left
                });
            }*/
        });
    
        
    /**
     * mouseenter or hover on node show actio-menu
     */    
    $(document).delegate(".node", 'mouseenter ', function ( event ) {        
        
        var content = '<div id="node-actions">\n\
                            <div class="menu open-menu">+</div>\n\
                            <ul class="action-menu">\n\
                                <li>\n\
                                    Add Relative <span class="icon-arrow-right"></span>\n\
                                    <ul class="sub-menu">\n\
                                        <li id="add-mother">Mother</li>\n\
                                        <li id="add-father">Father</li>\n\
                                        <li id="add-sister">Sister</li>\n\
                                        <li id="add-brother">Brother</li>\n\
                                    </ul>\n\
                                </li>\n\
                                <li>\n\
                                    Change Gender <span class="icon-arrow-right"></span>\n\
                                    <ul class="sub-menu">\n\
                                        <li>Male</li>\n\
                                        <li>Female</li>\n\
                                        <li>Unknown</li>\n\
                                    </ul>\n\
                                </li>\n\
                                <li>Delete</li>\n\
                            </ul>\n\
                        </div>';
        
        $(this).append(content);
        
    });
    
    /**
     *  mouseleave or hover over hide node-action menu
     */
    $(document).delegate(".node", 'mouseleave ', function ( event ) {
        $(this).find('#node-actions').remove();
    });
    
    /**
     * Action menu dropdown show/hide on clik to menu icon
     */ 
    $(document).delegate(".menu.open-menu", 'click', function ( event ) {        
        $(this).parent().find('.action-menu').show();
        $(this).parent().find('.menu.open-menu').removeClass('open-menu').addClass('close-menu').html('-');
    });
    $(document).delegate(".menu.close-menu", 'click', function ( event ) {        
        $(this).parent().find('.action-menu').hide();
        $(this).parent().find('.menu.close-menu').removeClass('close-menu').addClass('open-menu').html('+');
    });
    
    
    /**
     *  add new node functions
     */    
    $(document).delegate("#add-mother", 'click', function ( event ) {   
        
        var thisParentNode = $(this).closest( ".node" );
        var thisParentID = thisParentNode.attr('id');
        var parentPosition = thisParentNode.position();
        
        console.log(parentPosition);
        
        var newNode = crateNewNode('female');
        
        $(newNode).insertBefore(thisParentNode).draggable({
                        drag: function(e, ui) {
                            
                        }
                        //,containment: '.drop-container'
                        ,refreshPositions: true
                        ,snap: true
            });
        
        repositionNodes();
    });
    
    
    
    
    function repositionNodes(){
        console.log('reposition');
    }
    
    function crateNewNode( gender ){
        var content = '<div class="node '+gender+'">\n\
                        <p>Enter Name Here</p>\n\
                    </div>';
        return content;
    }
    
});