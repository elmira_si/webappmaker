<?php 

$db = 'webappmaker'; //all will chenge later

//all defoult queries for auto run

$pagesQ = 
"CREATE TABLE pages (
    `ID` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `title` TEXT COLLATE utf8mb4_unicode_ci NOT NULL,
    `content` LONGTEXT COLLATE utf8mb4_unicode_ci NOT NULL,
    `status` VARCHAR(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
    `order` INT(11) NOT NULL DEFAULT '0',
    `author` BIGINT(20) UNSIGNED NOT NULL DEFAULT '0',
    `parent` BIGINT(20) UNSIGNED NOT NULL DEFAULT '0',
    `created_at` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
    `last_modified` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00'
    ) 
ENGINE=INNODB 
AUTO_INCREMENT=1 
DEFAULT CHARSET=utf8mb4 
COLLATE=utf8mb4_unicode_ci; ";


$menuesQ= "CREATE TABLE menues (
    `ID` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `parent` BIGINT(20) UNSIGNED NOT NULL DEFAULT '0',
    `title` TEXT COLLATE utf8mb4_unicode_ci NOT NULL,
    `content` LONGTEXT COLLATE utf8mb4_unicode_ci NOT NULL,
    `status` VARCHAR(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
    `order` INT(11) NOT NULL DEFAULT '0',
    `author` BIGINT(20) UNSIGNED NOT NULL DEFAULT '0',    
    `created_at` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
    `last_modified` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00'
    ) 
ENGINE=INNODB 
AUTO_INCREMENT=1 
DEFAULT CHARSET=utf8mb4 
COLLATE=utf8mb4_unicode_ci;";

$forms = "CREATE TABLE `forms` (
  `ID` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` TEXT COLLATE utf8mb4_unicode_ci NOT NULL,
  `htmlObj` LONGTEXT COLLATE utf8mb4_unicode_ci NOT NULL,
  `jsonObj` LONGTEXT COLLATE utf8mb4_unicode_ci NOT NULL,  
  `order` INT(11) NOT NULL DEFAULT '0',
  `author` BIGINT(20) UNSIGNED NOT NULL DEFAULT '0',  
  `created_at` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ID`)
) 
ENGINE=INNODB 
AUTO_INCREMENT=1 
DEFAULT CHARSET=utf8mb4 
COLLATE=utf8mb4_unicode_ci;";




