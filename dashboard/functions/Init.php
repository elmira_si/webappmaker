<?php

class Init{
    
    public $layout = 'main';
    protected $controller = 'home';
    protected $method = 'index';
    protected $params = [];
	

    public function __construct( $config=NULL, $commonFunctions =NULL )
    {      
        
        if ( $config ) {
            require_once $config;
        }
        if ($commonFunctions){
            require_once $commonFunctions;
        }
                  
        require_once '../common/classes/db.class.php';   
        require_once '../common/includes/functions.php';   
        require_once 'Actions.php'; 
        require_once 'includes/index.php';        
         
        //self::testDB();
    }
    
   

    public function testDB(){
        $db = Db::getInstance();
        $table = "forms"; 
        
        $result = $db->selectAll($table);
        
        
        
        $data = array(
            'title' => '1111',
            'htmlObj' => 'obj1',
            'jsonObj' => 'obj2',
            'order' => '15',
            'author' => '2',
            'created_at' => '0000-00-00 00:00:00',
            'last_modified' => '0000-00-00 00:00:00',
        );
        //$insert = $db->insert('forms', $data);
        
        /* Select queries return a resultset */
        if($result){
            // Cycle through results
           while ($row = $result->fetch_object()){
               var_dump($row);
           }
           // Free result set
           $result->close();
           $db->next_result();
       }
    }
  
    
	
}
